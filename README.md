Diffuse emission around Abell S1136
===================================

This project contains the script that was used in Section 3.2 of
Macgregor et al. 2024
(Bibcode:[2024PASA...41...50M](https://ui.adsabs.harvard.edu/abs/2024PASA...41...50M),
DOI:[10.1017/pasa.2024.36](https://doi.org/10.1017/pasa.2024.36), or
arXiv:[2406.09709](https://arxiv.org/abs/2406.09709)). The necessary
input data are publicly available at
https://doi.org/10.5281/zenodo.14693411

The `Makefile` of this project contains all the analysis steps and
creates the necessary PDF file and LaTeX macros for this part of the
project. The `report.tex` file of this repository contains the source
to build the PDF with text, tables and figure(s). You can run the
project like below (setting the absolute address of the input dataset
and build directory):

```shell
export INDIR=/path/to/directory/containing/input/images
export BDIR=/path/to/build/directory
make -j8
```

Before any analysis, the Makefile checks the SHA1 checksum of the
input image (to make sure that it is the pre-planned input) and the
version of the running Gnuastro (should be 0.15). You can see such
settings at the start of `Makefile` (under "Processing parameters").
