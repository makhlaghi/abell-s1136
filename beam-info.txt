# Beam information for the analysis of Abell 1136
#
# How to find the beam correction with an example of ASKAP 888MHz:
#    SUM_scaled = SUM / (1.1331*( (12.60*10.04)/(2*2) ) )
#    SUM_scaled = SUM / 35.8354206
#
# Column 1: FREQUENCY [MHz,    int16] Frequency of band
# Column 2: BEAM-X    [arcsec, f32  ] Beam size in the X direction
# Column 3: BEAM-Y    [arcsec, f32  ] Beam size in the Y direction
# Column 4: SCALE     [n/a,    f32  ] Scale factor
154       73.95       72.85      1.1331
185       62.56       61.65      1.1331
215       53.98       53.73      1.1331
888       12.60       10.04      1.1331
2100      6.312       3.051      1.1331
