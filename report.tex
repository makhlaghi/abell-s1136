\documentclass[two column]{article}

%% Load the analysis outputs as LaTeX macros.
\input{ma-macros.tex}

%% Usable space within page.
\usepackage[body={18cm, 25.5cm}]{geometry}

%% Set the font.
\usepackage[T1]{fontenc}
\usepackage{newtxtext,newtxmath}

%% Load the PGFPlots and BibLaTeX preambles.
\input{tex/preamble-pgfplots}
\input{tex/preamble-biblatex}

%% To avoid creating figures.
\newcommand{\mamakefigure}{1}

%% Set the title and author information.
\title{Detection and ASKAP integrated flux measurements of Abell S1136}
\author{Mohammad Akhlaghi (one section of Macgregor et al.)}
\date{\small Commit \texttt\maGitCommit{} of\\\texttt{https://gitlab.com/makhlaghi/abell-s1136}}

%% Start the paper's body.
\begin{document}
\maketitle

Flux measurements in the Abell S1136 galaxy cluster and its filamentary and diffuse components were done on the MWA Phase II at 154 MHz, 185 MHz, and 216 MHz, ASKAP at 888 MHz, and ATCA at 2100 MHz.
A cutout of the cluster within each of the frequencies can be seen in Figure \ref{fig:detection} (c)--(e).
It is clear that the spatial resolution of the MWA data are much lower than ASKAP and ATCA.
It was therefore necessary to measure the total cluster emission in two modes: low-resolution and high-resolution.
The results of all measurements are available in Table \ref{table:flux_properties}.
The low-resolution total measurements were used in determining the spectral index of the filaments, detailed in the next section.

The total flux high resolution measurement was only done on the 888 MHz and 2100 MHz data.
Regions with significant signal were detected and labelled using NoiseChisel\footnote{The following NoiseChisel options were changed compared to the default (using Gnuastro version \maGnuastroVersion): because radio data are already heavily smoothed, convolution was disabled with \texttt{\maNoiseChiselkernel}, the quantile-threshold was increased with \texttt{\maNoiseChiselqthresh} and the growth-threshold was decreased to \texttt{\maNoiseChiseldetgrowquant}.
Furthermore, to increase purity, we used \texttt{\maNoiseChiselsnquant}.} over the $\maDetectionMHz$ MHz image and measurements, over all filters/regions, were done with MakeCatalog \citep[NoiseChisel and MakeCatalog are part of GNU Astronomy Utilities, Gnuastro;][]{akhlaghi15,akhlaghi19}.
NoiseChisel uses very low thresholds to nonparametricaly detect extended signal with any shape to very low signal-to-noise ratio, S/N, levels \citep[e.g., outer S/N of 0.25 in][]{akhlaghi21}.
Figure \ref{fig:detection}(a) shows the NoiseChisel detection of the total flux of Abell S1136 as the green-shaded pixels, clearly showing an excess of diffuse light beyond the immediately visible bright filaments and cluster members.
Other detections in the image are masked (covered by black pixels) in Fig. \ref{fig:detection}(b), showing that such an extended and contiguous emission is unique to Abell S1136.
The grayscale background of Fig. \ref{fig:detection}(a) and (b) is the same as Fig. \ref{fig:detection}(f).
For the ATCA measurement, the binary output of NoiseChisel (with value 1 for cluster's signal and 0 for others) was warped into the pixel resolution of ATCA 2100 MHz and the flux under it was measured.

The total low resolution measurement was done on all the frequency bands, within the cyan bound box\footnote{The vertices of the low resolution box in Fig. \ref{fig:detection}(c)  are located at $(\maLowResPolyVaRA,\maLowResPolyVaDec)$, $(\maLowResPolyVbRA,\maLowResPolyVbDec)$, $(\maLowResPolyVcRA,\maLowResPolyVcDec)$, $(\maLowResPolyVdRA,\maLowResPolyVdDec)$.} shown in Fig. \ref{fig:detection}(c).
Abell S1136 and PKS 2333–318 are the only measurable sources in the MWA data and this box was defined to avoid the flux of the latter when measuring the flux of Abell S1136 based on the MWA data at 154 MHz (which has the lowest spatial resolution).
For other frequencies, the pixels under the same polygon (based on its vertice coordinates) were used for the measurement.

As discussed earlier within this paper, the Abell S1136 cluster also shows filamentary structures and a large diffuse area in the ASKAP $\maDetectionMHz$ MHz image.
Therefore in Table \ref{table:flux_properties} the emission within these sub-structures is also reported in the top four rows.
The boundary of the three filaments (shown as purple in Figure \ref{fig:detection}a) were defined based on a surface brightness threshold of $\maFilamentThresh$ \maDataUnit.
The diffuse region was also defined based the same threshold: only detected pixels with a value lower than this were used in the measurement.
This shows that diffuse emission constitutes $\maFracDiffuse$ of the total cluster emission in $\maDetectionMHz$ MHz.

However, the correlated noise in radio data is very strong and has directional properties.
This is clearly visible in Fig. \ref{fig:detection}(f) and (g).
It is therefore very important to quantify the significance of this diffuse signal in a robust manner.
To do this, the footprint of the region was randomly placed in parts of the image with similar noise properties.
Any random position where a single pixel of the displaced footprint overlaps with a detection is discarded.
For those that don't overlap with a detection, the sum of pixel values is stored.
The process is terminated when $\maUpNum$ successful measurements have been done.
The sigma-clipped median ($m_r$) and standard deviation ($\sigma_r$) of this random distribution are calculated and compared with the total sum within the region ($s$).
The significance can be quantified by $(s-m_r)/\sigma_r$.

Using the upper-limit method above, the full diffuse signal of Abell S1136 in $\maDetectionMHz$ MHz, had a significance of $\maDiffuseAllSig\sigma$.
To confirm that this high fraction is not due to a concentrated region (that may have been missed in the masking) and is mostly uniform, we manually divided the diffuse region into two, shown as blue and red in Fig. \ref{fig:detection}(b).
Repeating the same upper-limit processing above over these separate regions, we found significances of $\maDiffuseSigmaOne\sigma$ and $\maDiffuseSigmaTwo\sigma$ respectively.

\begin{figure*}[h]

  %% See if the figure
  \ifdefined\mamakefigure
  \input{tex/detection.tex}
  \else
  \includegraphics{images/detection.eps}
  \fi

  %% Add the caption.
  \vspace{-3mm}
  \caption{\footnotesize Detections in Abell S1136.
    (a) Green region shows the \textsf{NoiseChisel}-detected region.
    The purple regions show the pixels within each filament.
    (b) The diffuse region (defined as regions with a per-pixel flux density less than $\maFilamentThresh$ \maDataUnit) is manually divided into the shown blue and red regions to quantify the significance of signal in them ($\maDiffuseSigmaOne\sigma$ and $\maDiffuseSigmaTwo\sigma$ respectively).
    The black pixels show other detected pixels within this image.
    (c) to (g) show the images obtained by each telescope at the frequency shown. (c),(d),(e) are from MWA, (f) is from ASKAP, and (g) is from ATCA.
  }
  \label{fig:detection}
\end{figure*}

All the measurements were done with the MakeCatalog program of GNU Astronomy Utilities (Gnuastro).
In Gnuastro, blind detection \citep[i.e., labeling pixels/voxels, done by NoiseChisel;][]{akhlaghi15} is separate from measurement \citep{akhlaghi19}.
This modular design allows the complex measurements that were done in this study.
For example when measuring the total emission in low resolution or filament emission, NoiseChisel was never used.
In the former, the pixels within the box were labeled and fed into MakeCatalog for measurements and in the latter pixels above the certain threshold were labeled and used.
Due to the very complex nature of noise in each of the filters, the errors reported in Table \ref{table:flux_properties} were calculated using the same upper-limit method above: the reported error is the value of $\sigma_r$.

\begin{table*}
\centering
\caption{Flux densities and spectral indices of the Abell~S1136 emission components}
\label{table:flux_properties}
\renewcommand{\arraystretch}{1.45} % Increase line spacing for the + and -.
\begin{tabular}{@{}lcccccc@{}}
\hline
Source                            & Spectral index ($\alpha$) & \multicolumn{5}{c}{Integrated flux density (mJy)} \\
\hline
& ASKAP--ATCA                             & 154 MHz      & 185 MHz      & 215 MHz      & 888 MHz          & 2100 MHz \\
\hline
Northern filament   & $\maCatHiResSpecIndFilN^{+\maCatHiResSpecIndFilNP}_{-\maCatHiResSpecIndFilNN}$ & --  & -- & --  & $\maCatHiResFileeeN\pm\maCatHiResFileeeNe$  & $\maCatHiResFiltozzN\pm\maCatHiResFiltozzNe$ \\

BCG filament        & $\maCatHiResSpecIndFilB^{+\maCatHiResSpecIndFilBP}_{-\maCatHiResSpecIndFilBN}$ & --  & -- & --  & $\maCatHiResFileeeB\pm\maCatHiResFileeeBe$  & $\maCatHiResFiltozzB\pm\maCatHiResFiltozzBe$ \\

Southern Filament   & $\maCatHiResSpecIndFilS^{+\maCatHiResSpecIndFilSP}_{-\maCatHiResSpecIndFilSN}$ & --  & -- & --  & $\maCatHiResFileeeS\pm\maCatHiResFileeeSe$  & $\maCatHiResFiltozzS\pm\maCatHiResFiltozzSe$ \\

All diffuse regions & $\maCatHiResSpecIndDif^{+\maCatHiResSpecIndDifP}_{-\maCatHiResSpecIndDifN}$  & --  & -- & --  & $\maCatHiResDifeee\pm\maCatHiResDifeeee$    & $\maCatHiResDiftozz\pm\maCatHiResDiftozze$   \\

\hline

Total (High resolution)$^\text{a}$ & $\maCatHiResSpecIndAll^{+\maCatHiResSpecIndAllP}_{-\maCatHiResSpecIndAllN}$ & -- & -- & -- & $\maCatHiResAlleee\pm\maCatHiResAlleeee$ & $\maCatHiResAlltozz\pm\maCatHiResAlltozze$ \\

Total (Low resolution)$^\text{b}$  & $\maCatLowResSpecInd^{+\maCatLowResSpecIndP}_{-\maCatLowResSpecIndN}$   & $\maCatLowResoff\pm\maCatLowResoffe$ & $\maCatLowResoef\pm\maCatLowResoefe$ & $\maCatLowRestof\pm\maCatLowRestofe$ & $\maCatLowReseee\pm\maCatLowReseeee^c$ & $\maCatLowRestozz\pm\maCatLowRestozze$ \\
\hline
\end{tabular} \\
{\footnotesize
  \textit{Notes.}
  $^\text{a}$ Within green region of Fig. \ref{fig:detection}(a).
  $^\text{b}$ Within cyan integration box shown in Fig. \ref{fig:detection}(c).
  $^\text{c}$ The image is too crowded for successfully placing the footprint($^b$) in ``empty'' regions (to estimate error), the error should be near, but slightly larger than the high-resolution error ($\maCatHiResAlleeee$).
}
\renewcommand{\arraystretch}{1} % Reset line spacing to original.
\end{table*}



%% Tell BibLaTeX to put the bibliography list here.
\printbibliography
\end{document}
