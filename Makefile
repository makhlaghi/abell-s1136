# Detect the diffuse emission around Abell S1136 imaged within the EMU
# project within the paper by Peter Macgregor et al. To run the
# script, simply set the directory containing the input datasets
# (available on https://doi.org/10.5281/zenodo.14693411) as the
# 'INDIR' environment variable, and your desired "build directory" (to
# keep all the intermediate products) in the 'BDIR' environment
# variables like below. Just be sure to use the absolute path of the
# directories.
#
#     $ export INDIR=/path/to/input/data
#     $ export BDIR=/path/to/build/directory
#     $ make -j8
#
# You can also put the two variables within the same 'make' command:
#
#     $ make INDIR=/path/to/s1136_subimage_interp.fits \
#            BDIR=/path/to/build/directory
#
# The input datasets should have the following name convention:
#      abell-s1136-XXXXMHz.fits
# Where XXXX is the frequency (in MHz): 154, 185, 215, 888, 2100.
#
# Copyright (C) 2021-2025 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.





# Processing parameters.
bcg-radius-pix = 7
crop-width-deg = 11/60
gnuastro-version = 0.15
upperlimit-num-random = 10000
filament-thresh-jy-per-beam = 0.0001

# Coordinates of components.
bcg-ra             = 354.0688793
bcg-dec            = -31.6029437
abells1136-ra      = 354.0708333
abells1136-dec     = -31.6102777
filament-bcg-ra    = 354.0710076
filament-bcg-dec   = -31.6087839
filament-north-ra  = 354.0859245
filament-north-dec = -31.5800528
filament-south-ra  = 354.0663838
filament-south-dec = -31.6342524
diffuse-1-ra       = 354.0850996
diffuse-1-dec      = -31.6254125
diffuse-2-ra       = 354.0845748
diffuse-2-dec      = -31.6021066
diffuse-3-ra       = 354.1117624
diffuse-3-dec      = -31.5819118

# Low-resolution polygon
polygon-va-ra  =  354.1557171
polygon-va-dec = -31.5784480
polygon-vb-ra  =  354.0902068
polygon-vb-dec = -31.5335496
polygon-vc-ra  =  354.0093253
polygon-vc-dec = -31.6229449
polygon-vd-ra  =  354.0748476
polygon-vd-dec = -31.6693744

# Frequencies of input images used:
freq-det = 888
freq-lowres  = 154 185 215
freq-highres = $(freq-det) 2100
freq-all = $(freq-lowres) $(freq-highres)

# SHA-1 checksums of each input image.
sha1-154  = 482d1e13b56b1c3c267a5952de57dcafb94a202a
sha1-185  = b8082d625ecd518d0702c9689fbe41ad5bbe96da
sha1-215  = d38efb804d282140ab16d5b6b4debfc4f0567233
sha1-888  = 97ec724cfa766947ce4d830302fb5c295c361303
sha1-2100 = ecf5dd9862af12b4b8d102440b003751d1712046

# Types of high-resolution catalogs.
highres-cat-types = filaments diffuse all

# Random number generator base.
rng-seed-base = 1621530320




# Set the ultimate target ('all').
ifeq ($(words $(INDIR) $(BDIR)),2)
all: report.pdf
else
all:
	@echo "Two environment variables are necessary for this Makefile:"
	echo "  INDIR: Name of directory containing input images. "
	echo "  BDIR: directory to host temporary built products"
	echo "        (this directory will be built if it doesn't exist,"
	echo "         we recommend using an empty directory)."
	echo "For example:"
	echo "  make INDIR=/path/to/input/dataset BDIR=/path/to/build/directory"
	echo ""; exit 1
endif

# Make the build-directory and other basic sub-directories (if it
# doesn't already exist).
texdir=$(BDIR)/tex
outdir=$(BDIR)/out
imgdir=$(texdir)/img
adir=$(BDIR)/analysis
labdir=$(adir)/labels
tikzdir=$(texdir)/tikz
texmacros=$(texdir)/ma-macros.tex
$(BDIR):; mkdir $@
$(imgdir) $(tikzdir): | $(texdir); mkdir $@
$(texdir) $(adir) $(outdir): | $(BDIR); mkdir $@
$(foreach f,$(freq-all),$(adir)/$(f)) $(labdir): | $(adir); mkdir $@

# Basic Make settings.
.ONESHELL:
SHELL=bash
.SHELLFLAGS = -ec
.PHONY: all $(texmacros)




















# Sanity checks
# -------------
#
# Make sure that the data and software are the expected versions.
sanitycheck=$(adir)/sanity-check.txt
$(sanitycheck): $(foreach f,$(freq-all),$(INDIR)/abell-s1136-$(f)MHz.fits) \
                | $(adir)

        # Make sure the input data are correct.
	for freq in $(freq-all); do

          # Set the expected SHA1 checksum for this frequency.
	  case $$freq in
	    154)  sha1_exp=$(sha1-154);;
	    185)  sha1_exp=$(sha1-185);;
	    215)  sha1_exp=$(sha1-215);;
	    888)  sha1_exp=$(sha1-888);;
	    2100) sha1_exp=$(sha1-2100);;
	    *) echo "frequency $$freq is not recognized!"; exit 1;
	  esac

          # Set the filename and calculate the SHA1 checksum.
	  file=$(INDIR)/abell-s1136-"$$freq"MHz.fits
	  sha1_cal=$$(sha1sum $$file | awk '{print $$1}')
	  if ! [ $$sha1_exp = $$sha1_cal ]; then
	    echo "Contents don't match expected input SHA-1 checksum:"
	    echo "    File: $$file"
	    echo "    Expected   SHA-1: $$sha1_exp"
	    echo "    Calculated SHA-1: $$sha1_cal"
	    exit 1
	  fi
	done

        # Confirm version of Gnuastro.
	gnuastrover=$$(astfits --version | awk 'NR==1{print $$NF}')
	if [ $$gnuastrover = $(gnuastro-version) ]; then
	  echo "Data and software are good!" > $@
	else
	  printf "\n\nABORTING: this analysis was writted with Gnuastro "
	  printf "$(gnuastro-version). To ensure the same results and "
	  printf "avoid unexpected errors, please install this "
	  printf "version to run this Makefile. If you have a newer "
	  printf "version, you can install Gnuastro 0.15 from source "
	  printf "(https://ftp.gnu.org/gnu/gnuastro/gnuastro-0.15.tar.gz) "
	  printf "in a custom directory (for example "
	  printf "'~/.local/gnuastro-0.15'), and add this to your PATH "
	  printf "before calling this Makefile."; exit 1
	fi




















# Input preparation and Detection+Sky-subtraction
# -----------------------------------------------
#
# Make sure all inputs have the same standards (are 2D, in HDU number
# 1, counting from 0), and have an odd number of pixels in each
# dimension
inputs = $(foreach f,$(freq-all),$(adir)/$(f)/input.fits)
$(inputs): $(adir)/%/input.fits: $(sanitycheck) | $(adir)/%

        # Set the name of the input copy.
	copy=$(subst .fits,-copy.fits,$@)
	crop=$(subst .fits,-crop.fits,$@)
	input=$(INDIR)/abell-s1136-$*MHz.fits

        # Crop out the polygon to have a good flat noise and no
        # artifacts.
	case $* in

          # Filters that don't need any special attention.
	  154 | 185 | 215)
	    astarithmetic $$input -h0 --output=$$crop;;

	  888)
            # Do a basic crop (to clean the headers).
	    astcrop $$input -h0 --mode=img --section=:,: --output=$$copy

            # Remove the keywords related to the extra (and empty!)
            # dimensions.
	    astfits $$copy -h1 --update=WCSAXES,2 --delete=CUNIT3 \
	            --delete=CRPIX3 --delete=CRPIX4 \
	            --delete=CDELT3 --delete=CDELT4 \
	            --delete=CRVAL3 --delete=CRVAL4

            # Define the vertices and make the crop. We need a polygon
            # because the noise in the center is different from the
            # noise on the edges.
	    vertices="354.6465666,-30.6869339"
	    vertices="$$vertices:353.9922407,-30.8685913"
	    vertices="$$vertices:353.8365858,-31.3249890"
	    vertices="$$vertices:353.3211290,-31.6358082"
	    vertices="$$vertices:353.2202425,-32.2750800"
	    vertices="$$vertices:353.8614999,-32.4675484"
	    vertices="$$vertices:354.2399622,-32.1481206"
	    vertices="$$vertices:354.8682248,-32.0285584"
	    vertices="$$vertices:355.1892677,-31.3243445"
	    vertices="$$vertices:355.4410826,-30.8620766"
	   astcrop $$copy --mode=wcs --polygon=$$vertices -o$$crop;;

	  2100)
            # Use arithmetic to clean the extra headers before cropping.
	    astarithmetic $$input -h0 --output=$$copy

            # Mask the bad stars. We don't need to use a polygon here
            # because the noise is mostly flat.
	    mask1=$$(echo "1 353.7743460 -31.2681326 5 1300 0 85 0.7 nan 1")
	    mask2=$$(echo "1 354.4356182 -31.6010338 5 1000 0 85 0.7 nan 1")
	    printf "%s\n%s\n" "$$mask1" "$$mask2" \
	           | astmkprof --background=$$copy --mode=wcs --output=$$crop;;

	  *)
	    echo "Not a recognized frequency for cropping: '$*'"
	    exit 1;;
	esac

        # Make sure the image has an odd number of pixels in both
        # dimensions. This helps later in the scaling of the labels
        # for filaments, diffuse regions and the whole cluster.
	d1=$$(astfits $$crop -h1 | awk '/^NAXIS1/{printf ($$3%2) ? ":" : ":*-1"}')
	d2=$$(astfits $$crop -h1 | awk '/^NAXIS2/{printf ($$3%2) ? ":" : ":*-1"}')
	astcrop $$crop --mode=img --section=$$d1,$$d2 -o$@

        # Clean up (delete the temporary file of this step).
	rm -f $$copy





# Run NoiseChisel to get the detection map as well as a properly
# sky-subtracted input image.
noisechisel = $(subst input.fits,nc.fits, $(inputs))
$(noisechisel): $(adir)/%/nc.fits: $(adir)/%/input.fits

        # None of the images need any convolution kernel, they are
        # already smoothed due to the frequency-space analysis.
	options="--kernel=none"

        # Set the options for each frequency.
	case $* in

        # Filters that don't need any special attention.
	154 | 185 | 215)
          # --tilesize slightly decreased to allow more tiles because
          #   the images are small.
          #
          # --meanmedqdiff slightly increased (loosened) to accept
          #   more tiles for better estimation of thresholds (bad
          #   tiles are automatically rejected during the outlier
          #   rejection phase.
          #
          # --qthresh and --dthresh greatly increased because of the
          #   huge smoothing in these datasets (correlated noise).
	  options="$$options --tilesize=25,25 --meanmedqdiff=0.02 "
	  options="$$options --qthresh=0.75 --dthresh=1"
	  ;;

        # Filters that need special attention.
	888 | 2100)
          # --qthresh increased due to the smoothing that is already
          #   present.
          #
          # --snquant increased to improve purity: we aren't worried
          #   about completeness (missing smaller/fainter true
          #   detections) in the scenario of this work. But a low
          #   purity in this already-smoothed image can artificially
          #   expand the main target.
          #
          # --detgrowquant slightly decreased to grow the objects into
          #   the more diffuse components of the signal (which is the
          #   goal here).
	  options="$$options --qthresh=0.5 --snquant=0.999"
	  options="$$options --detgrowquant=0.85"

          # For the main detection filter, save the NoiseChisel
          # options to report in the paper.
	  if [ $* = $(freq-det) ]; then
	    echo "$$options" > $(adir)/$*/nc-options.txt
	  fi
	  ;;

	*)
	   echo "Not a recognized frequency for cropping: '$*'"
	   exit 1;;
	esac

        # Run NoiseChisel with given options.
	astnoisechisel $< $$options --output=$@




















# Label images
# ------------
#
# Labeled images (where pixels are given an integer label
# corresponding to the object they should be measured in) are later
# used for catalog production in the next phase.
#
# Labels of filaments.
labs-filaments=$(labdir)/filaments.fits
$(labs-filaments): $(noisechisel) | $(labdir)

        # The values image to use.
	values=$(adir)/$(freq-det)/nc.fits

        # First, apply the threshold on the Sky-subtracted image.
	labthresh=$(subst .fits,-thresh.fits,$@)
	astarithmetic $$values $(filament-thresh-jy-per-beam) gt \
	              2 connected-components -o$$labthresh

        # Create a circular aperture of radius 7 pixels on the galaxy
        # core that overlaps with the middle filament.
	bcgmask=$(subst .fits,-bcg-mask.fits,$@)
	echo "1 $(bcg-ra) $(bcg-dec) 5 $(bcg-radius-pix) 0 0 1 1 1" \
	     | astmkprof --mode=wcs --clearcanvas --mforflatpix \
	                 --background=$$values --type=uint8 \
	                 --output=$$bcgmask

        # Remove the circular BCG mask from the labeled image.
	laball=$(subst .fits,-lab-all.fits,$@)
	astarithmetic $$labthresh $$bcgmask 0 where -g1 --output=$$laball

        # Find the labels of the three filaments. To do this, we'll
        # put a tiny circle on a pre-defined position within each
        # detection and measure the total flux.
	catcheck=$(subst .fits,-check.txt,$@)
	labcheck=$(subst .fits,-check.fits,$@)
	echo "1 $(filament-north-ra) $(filament-north-dec) 5 2 0 0 1 1 1" \
	     > $$catcheck
	echo "2 $(filament-bcg-ra)   $(filament-bcg-dec)   5 2 0 0 1 2 1" \
	     >> $$catcheck
	echo "3 $(filament-south-ra) $(filament-south-dec) 5 2 0 0 1 3 1" \
	     >> $$catcheck
	astmkprof $$catcheck --mode=wcs --background=$$values --clearcanvas \
	          --type=uint8 --mforflatpix --output=$$labcheck

        # Find the labels of the three filaments in our catalog.
	labmatch=$(subst .fits,-match.txt,$@)
	astmkcatalog $$labcheck -h1 --valuesfile=$$laball --valueshdu=1 \
	             --ids --mean -o$$labmatch

        # Only keep the good labels, while setting them to the new
        # labels. To do this, first will increment their label by an
        # integer much larger than any possible detection, then we'll
        # subtract that intiger.
	fixed=100000
	labcond=$$(awk '!/^#/{printf "lab %d eq %d where ", $$2, \
	                             '$$fixed'+$$1}' \
	               $$labmatch)
	astarithmetic $$laball set-lab lab $$labcond set-changed \
	              changed changed $$fixed lt 0 where $$fixed - \
	              set-sub sub sub 0 le 0 where uint8 \
	              --output=$@

        # Clean up (delete temporary intermediate files).
	rm $$labthresh $$bcgmask $$laball $$catcheck $$labcheck $$labmatch





# Labeled image of full cluster in the detection image.
lab-all = $(labdir)/all.fits
lab-others = $(labdir)/others.fits
$(lab-all): $(noisechisel) | $(labdir)

        # Values image used.
	values=$(adir)/$(freq-det)/nc.fits

        # Put a small circle over the central position of Abell 1136
        # to use in identifying the label of the full Abell S1136
        # label within all the other detections.
	labcheck=$(subst .fits,-lab-check.fits,$@)
	echo "1 $(abells1136-ra) $(abells1136-dec) 5 10 0 0 1 1 1" \
	     | astmkprof --mode=wcs --background=$$values --clearcanvas \
	                 --backhdu=INPUT-NO-SKY --type=uint8 --mforflatpix \
	                 --output=$$labcheck

        # Pull out the main detection's pixels.
	astarithmetic $$values --hdu=DETECTIONS 2 connected-components \
	              set-det det $$labcheck not nan where set-masked \
	              masked masked 0 eq nan where unique set-id \
	              det det id ne 0 where 0 gt -o$@

        # Pull out the foot print of the rest of the detections in the
        # image.
	astarithmetic $$values --hdu=DETECTIONS 2 connected-components \
	              set-det det $$labcheck not nan where set-masked \
	              masked masked 0 eq nan where unique set-id \
	              det det id eq 0 where 0 gt -o$(lab-others)

        # Delete temporary files
	rm $$labcheck





# Labeled image of diffuse regions.
lab-diffuse = $(labdir)/diffuse.fits
$(lab-diffuse): $(lab-all)
	astarithmetic $(lab-all) -g1 --output=$@ \
	              $(adir)/$(freq-det)/nc.fits \
	              $(filament-thresh-jy-per-beam) gt 2 dilate 0 where





# Label of sub-regions in the diffuse region (to measure their
# significance).
lab-diffuse-sig = $(adir)/$(freq-det)/diffuse-sig-lab.fits
$(lab-diffuse-sig): $(lab-diffuse)

        # Make the surgical cuts to divide the diffuse region into
        # two sub-regions.
	cut=$(subst .fits,-cut.fits,$@)
	cutcat=$(subst .fits,-cut.txt,$@)
	echo "1 354.0806029 -31.6164322 5 20 0 150 0.05 1 1" > $$cutcat
	astmkprof $$cutcat --background=$(lab-diffuse) --backhdu=1 \
	          --mode=wcs --mforflatpix --clearcanvas --type=uint8 \
	          --output=$$cut

        # Apply the surgical cuts and label the separate components.
	labsep=$(subst .fits,-separate.fits,$@)
	astarithmetic $(lab-diffuse) $$cut 0 where \
	              1 connected-components -g1 --output=$$labsep

        # Put a small circle on each diffuse region to later help find
        # its ID.
	catcheck=$(subst .fits,-check.txt,$@)
	labcheck=$(subst .fits,-check.fits,$@)
	echo "1 $(diffuse-1-ra) $(diffuse-1-dec) 5 2 0 0 1 1 1"  > $$catcheck
	echo "2 $(diffuse-2-ra) $(diffuse-2-dec) 5 2 0 0 1 2 1" >> $$catcheck
	astmkprof $$catcheck --mode=wcs --background=$(lab-diffuse) \
	          --clearcanvas --type=uint8 --mforflatpix --output=$$labcheck

        # Find the labels of the two diffuse regions.
	labmatch=$(subst .fits,-match.txt,$@)
	astmkcatalog $$labcheck -h1 --valuesfile=$$labsep --valueshdu=1 \
	             --ids --mean -o$$labmatch

        # Only keep the good labels, while setting them to the new
        # labels. To do this, first will increment their label by an
        # integer much larger than any possible detection, then we'll
        # subtract that intiger.
	fixed=100000
	labcond=$$(awk '!/^#/{printf "lab %d eq %d where ", $$2, \
	                             '$$fixed'+$$1}' \
	               $$labmatch)
	astarithmetic $$labsep set-lab lab $$labcond set-changed \
	              changed changed $$fixed lt 0 where $$fixed - \
	              set-sub sub sub 0 le 0 where uint8 \
	              --output=$@

        # Clean up.
	rm $$cut $$cutcat $$labsep $$catcheck $$labcheck $$labmatch





# Warp the labels images into the pixel scale of other high-resolution
# frequencies.
labscaled=$(foreach t,$(highres-cat-types), \
            $(foreach f,$(filter-out $(freq-det),$(freq-highres)), \
              $(adir)/$(f)/$(t)-lab.fits) )
$(labscaled): $(adir)/%-lab.fits: $(inputs) \
              $(foreach t,$(highres-cat-types),$(labdir)/$(t).fits)

        # Extract the defining elements (frequency and type).
	freq=$(word 1,$(subst /, ,$*))
	type=$(word 2,$(subst /, ,$*))

        # Find the warping factor.
	r=$$(astfits $(adir)/$$freq/input.fits -h1 \
	             --pixelscale --quiet | awk '{print $$1}')
	frac=$$(astfits $(labdir)/$$type.fits -h1 --pixelscale --quiet \
	                  | awk '{print $$1/'$$r'}')

        # Warp the labeled image so it has the same pixel scale as
        # the input dataset.
	labscaled=$(subst .fits,-scaled.fits,$@)
	astwarp $(labdir)/$$type.fits --scale=$$frac \
	        --centeroncorner --output=$$labscaled

        # Multiply the scaled image by the square of the scale factor
        # so they become integers again.
	labcorrected=$(subst .fits,-corrected.fits,$@)
	v=$$(echo $$frac | awk '{print $$1*$$1}')
	astarithmetic $$labscaled $$v x uint8 -o$$labcorrected

        # Get the center of the actual image in RA/DEC.
	center=$$(astfits $(adir)/$$freq/input.fits -h1 \
	                  --skycoverage --quiet \
	                  | awk 'NR==1{printf "%s %s", $$1, $$2}' \
	                  | asttable -c'arith $$1 $$2 wcstoimg' \
	                             --wcsfile=$$labcorrected \
	                  | awk '{printf "%s,%s", $$1, $$2}')

        # Get the pixel width of this frequency's image.
	width=$$(astfits $(adir)/$$freq/input.fits -h1 --quiet \
	                 --keyvalue=NAXIS1,NAXIS2 \
	                 | awk '{printf "%s,%s", $$1, $$2}')

        # Put the labeled image into the pixel grid of the desired
        # frequency image.
	astcrop $$labcorrected --mode=img --center=$$center \
	        --width=$$width -o$@

        # Clean up the temporary files.
	rm $$labcorrected $$labscaled





# Keep a FITS copy of the input images and labels images, focused on
# the cluster.
lab-crop-input = $(outdir)/done.txt
lab-crop-det = $(outdir)/lab-detection.fits
lab-crop-others = $(outdir)/lab-others.fits
lab-crop-diffuse = $(outdir)/lab-diffuse.fits
lab-crop-filaments = $(outdir)/lab-filaments.fits
lab-crop-diffuse-sig = $(outdir)/lab-diffuse-sig.fits
$(lab-crop-input): $(labs-filaments) $(lab-diffuse) $(lab-all) \
                   $(lab-diffuse-sig) $(outdir)

        # Full detection map.
	astcrop $(lab-all) --center=$(abells1136-ra),$(abells1136-dec) \
	        --width=$(crop-width-deg) --output=$(lab-crop-det)

        # Filaments
	astcrop $(labs-filaments) --center=$(abells1136-ra),$(abells1136-dec) \
	        --width=$(crop-width-deg) --output=$(lab-crop-filaments)

        # Diffuse region
	astcrop $(lab-diffuse) --center=$(abells1136-ra),$(abells1136-dec) \
	        --width=$(crop-width-deg) --output=$(lab-crop-diffuse)

        # Other detections
	astcrop $(lab-others) --center=$(abells1136-ra),$(abells1136-dec) \
	        --width=$(crop-width-deg) --output=$(lab-crop-others)

        # Diffuse sub-regions (to report significance)
	astcrop $(lab-diffuse-sig) --center=$(abells1136-ra),$(abells1136-dec) \
	        --width=$(crop-width-deg) --output=$(lab-crop-diffuse-sig)

        # Crop of image in each frequency.
	for f in $(freq-all); do
	  astcrop $(adir)/$$f/input.fits --width=$(crop-width-deg) \
	          --center=$(abells1136-ra),$(abells1136-dec) \
	          --output=$(outdir)/$$f.fits
	done

        # Write final target (signifying that the job is done).
	echo "Job Done" > $@




















# Visualization (images)
# ----------------------
#
# Having the input images and labels, we now need to show the
# detections as JPEG images to insert into the paper.
# Visualize full NoiseChisel detection and filaments.
convertt-fluxlow  = -0.0002
convertt-fluxhigh =  0.0004





# Visualize full detection and filaments.
vis-full = $(imgdir)/abell-s1136-full-det.eps
$(vis-full): $(lab-crop-input) | $(imgdir)

        # Delete the TiKZ image so it is recreated in the paper when
        # this rule is re-run.
	rm -f $(tikzdir)/report-figure0.*

        # Mark the edge of the filaments over the full area.
	edge=$(imgdir)/filaments-edge.fits
	astarithmetic $(lab-crop-filaments) -h1 0 gt set-lab \
	              lab lab 2 erode 0 where --output=$$edge

        # Add it to the green color channel.
	base=$(outdir)/$(freq-det).fits
	green=$(imgdir)/detection-green.fits
	astarithmetic $$base $(lab-crop-det) 0.00008 x + set-i \
	              i $$edge -1 where -g1 --output=$$green

        # Convert it into a EPS (we don't need borders because it will
        # have axis labels.
	astconvertt $$base $$green $$base --borderwidth=0 \
	            --fluxlow=$(convertt-fluxlow) \
	            --fluxhigh=$(convertt-fluxhigh) \
	            -h1 -h1 -h1 --output=$@

        # Clean up.
	rm $$green $$edge $$ecrop





# Visualize only the diffuse region.
vis-diffuse = $(imgdir)/abell-s1136-diffuse.eps
$(vis-diffuse): $(lab-crop-input) | $(imgdir)

        # Delete the TiKZ image so it is recreated in the paper when
        # this rule is re-run.
	rm -f $(tikzdir)/report-figure0.*

        # Define the blue color channel for first diffuse region.
	base=$(outdir)/$(freq-det).fits
	blue=$(imgdir)/vis-diffuse-blue.fits
	astarithmetic $$base $(lab-crop-diffuse-sig) 1 eq \
	              0.0002 x + \
	              $(lab-crop-others) nan where \
	              -g1 --output=$$blue

        # Define the red color channel for second diffuse region.
	red=$(imgdir)/vis-diffuse-red.fits
	astarithmetic $$base $(lab-crop-diffuse-sig) 2 eq \
	              0.0002 x + \
	              $(lab-crop-others) nan where \
	              -g1 --output=$$red

        # Set the green color channel (base with other detection
        # masked).
	green=$(imgdir)/vis-diffuse-green.fits
	astarithmetic $$base $(lab-crop-others) nan where \
	              -g1 --output=$$green

        # Build them into a EPS.
	astconvertt $$red $$green $$blue --borderwidth=0 \
	            --fluxlow=$(convertt-fluxlow) \
	            --fluxhigh=$(convertt-fluxhigh) \
	            -h1 -h1 -h1 --output=$@

        # Clean up.
	rm $$red $$green $$blue





# Show images of all frequencies.
vis-all-freq = $(foreach f,$(freq-all),$(imgdir)/crop-$(f).eps)
$(vis-all-freq): $(imgdir)/crop-%.eps: $(lab-crop-input) | $(imgdir)

        # Delete the TiKZ image so it is recreated in the paper when
        # this rule is re-run.
	rm -f $(tikzdir)/report-figure0.*

        # Set the color range.
	case $* in
	  154)  fl=-0.03;               fh=0.1;;
	  185)  fl=-0.03;               fh=0.06;;
	  215)  fl=-0.02;               fh=0.04;;
	  888)  fl=$(convertt-fluxlow); fh=$(convertt-fluxhigh);;
	  2100) fl=-0.00005;            fh=0.0001;;
	  *) echo "frequency $$freq is not recognized!"; exit 1;
	esac

        # Convert the image to EPS.
	astconvertt $(outdir)/$*.fits -o$@ --fluxlow=$$fl --fluxhigh=$$fh





# Polygon coordinates over image.
vis-polygon = $(imgdir)/polygon-for-low-res.tex
$(vis-polygon): $(lab-crop-input) | $(imgdir)

        # Width of finally displayed image.
	fraclinewidth=0.195

        # Small offset introduced when inserting the image in the
        # final figure.
	xoffset=0.005

        # Reference cropped image (any one of the crops of the inputs
        # is fine, the pixel scales are irrelevant).
	base=$(outdir)/888.fits

        # Delete the TiKZ image so it is recreated in the paper when
        # this rule is re-run.
	rm -f $@
	rm -f $(tikzdir)/report-figure0.*

        # First, get the pixel coordiantes of each vertice
	axy=$$(echo "$(polygon-va-ra) $(polygon-va-dec)" \
	            | asttable -c'arith $$1 $$2 wcstoimg' --wcsfile=$$base)
	bxy=$$(echo "$(polygon-vb-ra) $(polygon-vb-dec)" \
	            | asttable -c'arith $$1 $$2 wcstoimg' --wcsfile=$$base)
	cxy=$$(echo "$(polygon-vc-ra) $(polygon-vc-dec)" \
	            | asttable -c'arith $$1 $$2 wcstoimg' --wcsfile=$$base)
	dxy=$$(echo "$(polygon-vd-ra) $(polygon-vd-dec)" \
	            | asttable -c'arith $$1 $$2 wcstoimg' --wcsfile=$$base)

        # Get the full pixel size of the image.
	wx=$$(astfits $$base -h1 | awk '/^NAXIS1/{print $$3}')
	wy=$$(astfits $$base -h1 | awk '/^NAXIS2/{print $$3}')

        # Convert the pixel coordinates of each vertice to fractions
        # of '\linewidth' based on the fact that each displayed image
        # is has a width of 'fraclinewidth':
        #
        #      wx   --> fraclinewidth  |  X = (ax*fraclinewidth)/wx
        #      ax   --> X              |
	v=$$fraclinewidth
	echo "\newcommand{\maLowResPolyWidth}{$$v}" >> $@
	v=$$(echo $$axy | awk '{print $$1*'$$fraclinewidth'/'$$wx'+'$$xoffset'}')
	echo "\newcommand{\maLowResPolyAx}{$$v}" >> $@
	v=$$(echo $$axy | awk '{print $$2*'$$fraclinewidth'/'$$wy'+'$$xoffset'}')
	echo "\newcommand{\maLowResPolyAy}{$$v}" >> $@
	v=$$(echo $$bxy | awk '{print $$1*'$$fraclinewidth'/'$$wx'+'$$xoffset'}')
	echo "\newcommand{\maLowResPolyBx}{$$v}" >> $@
	v=$$(echo $$bxy | awk '{print $$2*'$$fraclinewidth'/'$$wy'+'$$xoffset'}')
	echo "\newcommand{\maLowResPolyBy}{$$v}" >> $@
	v=$$(echo $$cxy | awk '{print $$1*'$$fraclinewidth'/'$$wx'+'$$xoffset'}')
	echo "\newcommand{\maLowResPolyCx}{$$v}" >> $@
	v=$$(echo $$cxy | awk '{print $$2*'$$fraclinewidth'/'$$wy'+'$$xoffset'}')
	echo "\newcommand{\maLowResPolyCy}{$$v}" >> $@
	v=$$(echo $$dxy | awk '{print $$1*'$$fraclinewidth'/'$$wx'+'$$xoffset'}')
	echo "\newcommand{\maLowResPolyDx}{$$v}" >> $@
	v=$$(echo $$dxy | awk '{print $$2*'$$fraclinewidth'/'$$wy'+'$$xoffset'}')
	echo "\newcommand{\maLowResPolyDy}{$$v}" >> $@





# Sky coverage of image.
vis-sky-coverage = $(imgdir)/sky-coverage.tex
$(vis-sky-coverage): $(lab-crop-input) | $(imgdir)

        # Calculate the sky coverage.
	coverage=$$(astfits $(outdir)/888.fits --skycoverage --quiet \
	                    | awk 'NR==2')
	v=$$(echo $$coverage | awk '{print $$1}')
	echo "\newcommand{\maCropRAMin}{$$v}"   > $@
	v=$$(echo $$coverage | awk '{print $$2}')
	echo "\newcommand{\maCropRAMax}{$$v}"  >> $@
	v=$$(echo $$coverage | awk '{print $$3}')
	echo "\newcommand{\maCropDecMin}{$$v}" >> $@
	v=$$(echo $$coverage | awk '{print $$4}')
	echo "\newcommand{\maCropDecMax}{$$v}" >> $@
	v=$$(echo $$coverage | awk '{print ($$2-$$1)/7}')
	echo "\newcommand{\maTickDist}{$$v}"   >> $@



















# Measurements (creating catalogs)
# --------------------------------
#
# Calculate the beam correction factor from the 'beam-info.txt' file
# and the pixel scale of the given frequency. But before that we need
# to calculate the pixel area in arcsec^2.
beam-correction = $(foreach f,$(freq-all),$(adir)/$(f)/beam-correction.txt)
$(beam-correction): $(adir)/%/beam-correction.txt: $(adir)/%/input.fits
	pixarea=$$(astfits $< --pixelscale --quiet \
	                   | awk '{print $$1*3600*$$2*3600}')
	asttable beam-info.txt --equal=FREQUENCY,$* \
	         -c'arith BEAM-X BEAM-Y x '$$pixarea' / SCALE x' \
	         > $@





# Set the random number generator seed for each catalog so the seeds
# aren't identical for each catalog (for a reproducible result). Each
# filter will have five unique seeds (the detection high-resolution
# filters needs all five, the other high-resolution image needs 4, but
# the low-resolution ones need only 1).
rng-seed = $(subst beam-correction.txt,rng-seed.txt,$(beam-correction))
$(rng-seed): $(adir)/%/rng-seed.txt: | $(adir)/%
	case $* in
	  154)  a=0;;
	  185)  a=1;;
	  215)  a=2;;
	  888)  a=3;;
	  2100) a=4;;
	  *) echo "Frequency $$freq not recognized!"; exit 1;;
	esac
	echo $(rng-seed-base) $$a \
	     | awk '{for(i=0;i<5;++i) print $$1+($$2*5)+i}' > $@





# Three catalogs for high-resolution frequencies:
#   - Filaments.
#   - Diffuse region.
#   - Full cluster.
cat-comments-id=Object identifier.
cat-comments-bright=Scaled sum of pixels.
cat-comments-up1sig=One sigma value of all random measurements.
cat-high-res = $(foreach t,filaments diffuse all, \
                 $(foreach f,$(freq-highres),$(adir)/$(f)/$(t)-cat.tex))
$(cat-high-res): $(adir)/%-cat.tex: $(lab-diffuse) $(lab-all) \
                 $(labs-filaments) $(beam-correction) $(rng-seed) \
                 $(labscaled)

        # Extract the defining elements (frequency and type).
	freq=$(word 1,$(subst /, ,$*))
	type=$(word 2,$(subst /, ,$*))

        # Set the type/frequency related parameters. In LaTeX we can't
        # use numbers in the variable names, so I'll use the first
        # character of each digit in the number: 888 --> eight eight
        # eight --> eee.
	case $$type in
	  all)       line=1; tstr=All;;
	  diffuse)   line=2; tstr=Dif;;
	  filaments) line=3; tstr=Fil;;
	  *) echo "Type $$type not recognized!"; exit 1;;
	esac
	case $$freq in
	  888)  fstr=eee;;
	  2100) fstr=tozz;;
	esac

        # If we are not on the detection frequency, then center and
        # warp the label into the grid of of the original data.
	lab=$(subst .tex,-lab.fits,$@)
	if [ x$$freq = x$(freq-det) ]; then
	  ln -fs $(labdir)/$$type.fits $$lab
	else
	  ln -fs $(adir)/$$freq/$$type-lab.fits $$lab
	fi

        # Fix the random number generator seed (the 1st, 2nd and 3rd
        # rows are reserved for the high-resolution measurements).
	seed=$$(awk 'NR=='$$line'' $(adir)/$$freq/rng-seed.txt)
	export GSL_RNG_SEED=$$seed

        # Generate the raw catalog.
	nc=$(adir)/$$freq/nc.fits
	icat=$(subst .tex,-init.txt,$@)
	astmkcatalog $$lab -h1 --output=$$icat --envseed \
	             --instd=$$nc      --stdhdu=SKY_STD \
	             --valuesfile=$$nc --valueshdu=INPUT-NO-SKY \
	             --upmaskfile=$$nc --upmaskhdu=DETECTIONS \
	             --ids --brightness --upperlimitonesigma \
	             --upperlimitsigma --upnum=$(upperlimit-num-random)

        # Convert the units to mJy (correct for the beam). For the
        # brightness error, we'll use the S/N column and divide the
        # new brightness by the S/N to get the raw error.
	cat=$(subst .tex,.txt,$@)
	beamcorr=$$(cat $(adir)/$$freq/beam-correction.txt)
	asttable $$icat -cOBJ_ID \
	         -c'arith BRIGHTNESS           1000 x '$$beamcorr' /' \
	         -c'arith UPPERLIMIT_ONE_SIGMA 1000 x '$$beamcorr' /' \
	         --colmetadata=OBJ_ID,ID,counter,"$(cat-comments-id)" \
	         --colmetadata=2,BRIGHTNESS,mJy,"$(cat-comments-b)" \
	         --colmetadata=3,UP_ONE_SIG,mJy,"$(cat-comments-up1sig)" \
	         -cUPPERLIMIT_SIGMA --output=$$cat

        # Write the catalog as LaTeX macros to easily insert into the
        # paper. Because there are three filaments, it is necessary to
        # treat them separately.
	rm -f $@
	if [ $$type = filaments ]; then
	  asttable $$cat -cID -cBRIGHTNESS -cUP_ONE_SIG \
	     | while read i b e; do
	         case $$i in 1) nstr=N;; 2) nstr=B;; 3) nstr=S;; esac
	         v=$$(printf "%.2f" $$b)
	         echo "\newcommand{\maCatHiRes$$tstr$$fstr$$nstr}{$$v}" >> $@
	         v=$$(printf "%.2f" $$e)
	         echo "\newcommand{\maCatHiRes$$tstr$$fstr$$nstr"e"}{$$v}" >> $@
	       done
	else
	  asttable $$cat -cBRIGHTNESS -cUP_ONE_SIG \
	     | while read b e; do
	         v=$$(printf "%.2f" $$b)
	         echo "\newcommand{\maCatHiRes$$tstr$$fstr}{$$v}" >> $@
	         v=$$(printf "%.2f" $$e)
	         echo "\newcommand{\maCatHiRes$$tstr$$fstr"e"}{$$v}" >> $@
	       done
	fi

        # Clean up.
	rm $$lab $$icat





# Significance of diffuse emission in detection frequency by random
# placing over the image.
diffuse-sig = $(adir)/$(freq-det)/diffuse-sig.tex
$(diffuse-sig): $(lab-diffuse-sig) $(adir)/$(freq-det)/rng-seed.txt

        # Fix the random number generator seed (the 4-th row is
        # reserved for the diffuse region's catalogs).
	seed=$$(awk 'NR==4' $(adir)/$(freq-det)/rng-seed.txt)
	export GSL_RNG_SEED=$$seed

        # Calculate the over-all catalog over the cropped image
	cat=$(subst .tex,.txt,$@)
	nc=$(adir)/$(freq-det)/nc.fits
	astmkcatalog $(lab-diffuse-sig) -h1 --output=$$cat --envseed \
	             --valuesfile=$$nc --valueshdu=INPUT-NO-SKY \
	             --upmaskfile=$$nc --upmaskhdu=DETECTIONS \
	             --upnum=$(upperlimit-num-random) \
	             --ids --upperlimitsigma

        # Write the LaTeX macros
	rm -f $@
	asttable $$cat -cOBJ_ID -cUPPERLIMIT_SIGMA \
	   | while read i s; do
	       case $$i in 1) nstr=One;; 2) nstr=Two;; esac
	       v=$$(printf "%.2f" $$s)
	       echo "\newcommand{\maDiffuseSigma$$nstr}{$$v}" >> $@
	     done





# Low resolution catalog (values within a polygon).
cat-low-res = $(foreach f,$(freq-all),$(adir)/$(f)/low-res-cat.tex)
$(cat-low-res): $(adir)/%/low-res-cat.tex: $(adir)/%/nc.fits

        # To make the box label, we need to start with an image where
        # all the pixels are zero.
	nc=$(adir)/$*/nc.fits
	zeros=$(subst .tex,-zeros.fits,$@)
	astarithmetic $$nc nan eq -o$$zeros

        # Cutout the rectangle containging the whole flux (pixels
        # inside the polygon will have NaN values).
	polygon="$(polygon-va-ra),$(polygon-va-dec)"
	polygon="$$polygon:$(polygon-vb-ra),$(polygon-vb-dec)"
	polygon="$$polygon:$(polygon-vc-ra),$(polygon-vc-dec)"
	polygon="$$polygon:$(polygon-vd-ra),$(polygon-vd-dec)"
	polygonout=$(subst .tex,-polygon-out.fits,$@)
	astcrop $$zeros --mode=wcs --polygon=$$polygon \
	         --polygonout --output=$$polygonout

        # Set all the NaN pixels to 1.
	lab=$(subst .tex,-lab.fits,$@)
	astarithmetic $$polygonout isblank -o$$lab

        # Fix the random number generator seed (the 5-th row is
        # reserved for the low-resolution catalog).
	export GSL_RNG_SEED=$$(awk 'NR==5' $(adir)/$*/rng-seed.txt)

        # Generate the raw catalog.
	icat=$(subst .tex,-init.txt,$@)
	astmkcatalog $$lab -h1 --output=$$icat --envseed \
	             --instd=$$nc      --stdhdu=SKY_STD \
	             --valuesfile=$$nc --valueshdu=INPUT-NO-SKY \
	             --upmaskfile=$$nc --upmaskhdu=DETECTIONS \
	             --ids --brightness --upperlimitonesigma \
	             --upperlimitsigma --upnum=$(upperlimit-num-random)

        # Convert the units to mJy (correct for the beam). For the
        # brightness error, we'll use the S/N column and divide the
        # new brightness by the S/N to get the raw error.
	cat=$(subst .tex,.txt,$@)
	beamcorr=$$(cat $(adir)/$*/beam-correction.txt)
	asttable $$icat -cOBJ_ID \
	         -c'arith BRIGHTNESS           1000 x '$$beamcorr' /' \
	         -c'arith UPPERLIMIT_ONE_SIGMA 1000 x '$$beamcorr' /' \
	         --colmetadata=OBJ_ID,ID,counter,"$(cat-comments-id)" \
	         --colmetadata=ARITH_2,BRIGHTNESS,mJy,"$(cat-comments-b)" \
	         --colmetadata=ARITH_4,UP_ONE_SIG,mJy,"$(cat-comments-up1sig)" \
	         -cUPPERLIMIT_SIGMA --output=$$cat

        # String to use in LaTeX macro. In LaTeX we can't use numbers
        # in the variable names, so I'll use the first character of
        # each digit in the number: 888 --> eight eight eight --> eee.
	case $* in
	  154)  fstr=off;;
	  185)  fstr=oef;;
	  215)  fstr=tof;;
	  888)  fstr=eee;;
	  2100) fstr=tozz;;
	esac

        # Convert the table into LaTeX macros to insert in paper.
	rm -f $@
	asttable $$cat -cBRIGHTNESS -cUP_ONE_SIG \
	   | while read b e; do
	       v=$$(printf "%.2f" $$b)
	       echo "\newcommand{\maCatLowRes$$fstr}{$$v}" >> $@
	       v=$$(printf "%.2f" $$e)
	       echo "\newcommand{\maCatLowRes$$fstr"e"}{$$v}" >> $@
	     done

        # Clean up.
	rm $$zeros $$polygonout $$lab $$icat





# Spectral index measurements (in 888MHz in relation to the
# 2100MHz). The spectral index for the integrated flux is measured
# like below (assuming the integrated flux of ASKAP@888MHz is called
# 'A', and the integrated flux of ATCA@2100MHz is called 'B'):
#
#    SI = log(A/B) / log(888/2100)
#
# For the error in spectral index let's take the error in 'A' to be
# 'DA' and the error in 'B' to be 'DB'. So:
#
#    Aa = A - DA                   Ba = B - DB
#    Ab = A + DA                   Bb = B + DB
#
# We can now define two spectral indexs:
#
#    SIa = log(Aa/Bb) / log(888/2100)
#    SIb = log(Ab/Ba) / log(888/2100)
#
# where SIa > SI > SIb (because it is negative!). So we can now
# estimate the positive and negative error to be
#
#    Positive error: SIb - SI
#    Negative error: SI  - SIa
#
# This method is used after consulation with Peter Macgregor.
spectral-indexs = $(foreach t,filaments diffuse all low-res, \
                    $(adir)/$(freq-det)/$(t)-spec-ind.tex)
$(spectral-indexs): $(adir)/$(freq-det)/%-spec-ind.tex: \
                    $(cat-high-res) $(cat-low-res)

        # Set the frequencies
	freqhigh=2100
	freqlow=$(freq-det)

        # Set the catalog names.
	Acat=$(adir)/$$freqlow/$*-cat.txt
	Bcat=$(adir)/$$freqhigh/$*-cat.txt

        # Set custom parameters. Note that for the 'all' and 'low-res'
        # modes, we have significant signal to noise, so there is no
        # need to use the upper-limit.
	case $* in
	  all)       tstr=All; rstr=Hi  ;;
	  low-res)   tstr="";  rstr=Low ;;
	  diffuse)   tstr=Dif; rstr=Hi  ;;
	  filaments) tstr=Fil; rstr=Hi  ;;
	  *) echo "Type $$type not recognized!"; exit 1;;
	esac

        # Measure the spectral index and its positive/negative
        # error. See the comments above for the derivation of the
        # equations.
	spcat=$(subst .tex,.txt,$@)
	awk '!/^#/ && NR==FNR{ A[++c]=$$2; Ae[c]=$$3; } \
	     !/^#/ && NR!=FNR{ B[++d]=$$2; Be[d]=$$3; } \
	     END{logfreqfrac=log('$$freqlow'/'$$freqhigh'); \
	         for(i=1; i<=c; ++i) \
	         { \
	           SI=log(   A[i]       / B[i]        ) / logfreqfrac; \
	           SIa=log( (A[i]-Ae[i])/(B[i]+Be[i]) ) / logfreqfrac; \
	           SIb=log( (A[i]+Ae[i])/(B[i]-Be[i]) ) / logfreqfrac; \
	           print i, SI, SI-SIb, SIa-SI; \
	         } }' \
	     $$Acat $$Bcat > $$spcat

        # Write the values as LaTeX macros.
	rm -f $@
	if [ $* = filaments ]; then
	  cat $$spcat | while read i s ep en; do
	    case $$i in 1) nstr=N;; 2) nstr=B;; 3) nstr=S;; esac
	    v=$$(printf "%.2f" $$s)
	    echo "\newcommand{\maCat"$$rstr"ResSpecInd$$tstr$$nstr}{$$v}" >> $@
	    v=$$(printf "%.2f" $$ep)
	    echo "\newcommand{\maCat"$$rstr"ResSpecInd$$tstr$$nstr"P"}{$$v}">>$@
	    v=$$(printf "%.2f" $$en)
	    echo "\newcommand{\maCat"$$rstr"ResSpecInd$$tstr$$nstr"N"}{$$v}">>$@
	  done
	else
	  cat $$spcat | while read i s ep en; do
	    v=$$(printf "%.2f" $$s)
	    echo "\newcommand{\maCat"$$rstr"ResSpecInd$$tstr}{$$v}" >> $@
	    v=$$(printf "%.2f" $$ep)
	    echo "\newcommand{\maCat"$$rstr"ResSpecInd$$tstr"P"}{$$v}" >> $@
	    v=$$(printf "%.2f" $$en)
	    echo "\newcommand{\maCat"$$rstr"ResSpecInd$$tstr"N"}{$$v}" >> $@
	  done
	fi

        # Clean up.
	rm $$spcat
















# Preparing Report
# ----------------
#
# LaTeX macros to used in report.
$(texmacros): $(cat-high-res) \
              $(cat-low-res) \
              $(diffuse-sig) \
              $(lab-crop-input) \
              $(vis-full) \
              $(vis-diffuse) \
              $(vis-all-freq) \
              $(vis-polygon) \
              $(vis-sky-coverage) \
              $(spectral-indexs) \
              | $(texdir)

        # Git commit hash (necessary to describe point in history).
	v=$$(git describe --dirty --always --long)
	echo "\newcommand{\maGitCommit}{$$v}" > $@.tmp

        # Write the processing parameters.
	v=$(filament-thresh-jy-per-beam)
	echo "\newcommand{\maFilamentThresh}{$$v}"                >> $@.tmp
	echo "\newcommand{\maDetectionMHz}{$(freq-det)}"          >> $@.tmp
	echo "\newcommand{\maUpNum}{$(upperlimit-num-random)}"    >> $@.tmp
	echo "\newcommand{\maLowResPolyVaRA}{$(polygon-va-ra)}"   >> $@.tmp
	echo "\newcommand{\maLowResPolyVaDec}{$(polygon-va-dec)}" >> $@.tmp
	echo "\newcommand{\maLowResPolyVbRA}{$(polygon-vb-ra)}"   >> $@.tmp
	echo "\newcommand{\maLowResPolyVbDec}{$(polygon-vb-dec)}" >> $@.tmp
	echo "\newcommand{\maLowResPolyVcRA}{$(polygon-vc-ra)}"   >> $@.tmp
	echo "\newcommand{\maLowResPolyVcDec}{$(polygon-vc-dec)}" >> $@.tmp
	echo "\newcommand{\maLowResPolyVdRA}{$(polygon-vd-ra)}"   >> $@.tmp
	echo "\newcommand{\maLowResPolyVdDec}{$(polygon-vd-dec)}" >> $@.tmp

        # NoiseChisel detection parameters.
	v=$$(astnoisechisel --version | awk 'NR==1{print $$NF}')
	echo "\newcommand{\maGnuastroVersion}{$$v}" >> $@.tmp
	for opt in $$(cat $(adir)/$(freq-det)/nc-options.txt); do
	  o=$$(echo "$$opt" | sed -e's/--/-\{\}-/');
	  name=$$(echo "$$opt" | sed -e's/--//' -e's/=/ /' | awk '{print $$1}')
	  echo "\newcommand{\maNoiseChisel$$name}{$$o}" >> $@.tmp
	done

        # Fraction of flux in diffuse signal.
	a=$$(asttable $(adir)/$(freq-det)/all-cat.txt -cBRIGHTNESS)
	d=$$(asttable $(adir)/$(freq-det)/diffuse-cat.txt -cBRIGHTNESS)
	v=$$(echo $$d $$a | awk '{printf "%.2f", $$1/$$2}')
	echo "\newcommand{\maFracDiffuse}{$$v}" >> $@.tmp

        # Significance of diffuse sigma
	v=$$(asttable $(adir)/$(freq-det)/diffuse-cat.txt \
	              -cUPPERLIMIT_SIGMA \
	              | awk '{printf "%.2f", $$1}')
	echo "\newcommand{\maDiffuseAllSig}{$$v}" >> $@.tmp

        # Write the already-written macros.
	cat $(vis-polygon) >> $@.tmp
	cat $(adir)/*/*.tex >> $@.tmp
	cat $(vis-sky-coverage) >> $@.tmp

        # Calculate the total significance of diffuse emission.
	v=$$(asttable $(adir)/$(freq-det)/diffuse-cat.txt \
	              -cUPPERLIMIT_SIGMA \
	              | awk '{printf "%.2f", $$1}')
	echo "\newcommand{\maDiffuseSigmaAll}{$$v}" >> $@.tmp

        # Units of detection image.
	v=$$(astfits $(adir)/$(freq-det)/input.fits -h1 \
	             | grep ^BUNIT | sed -e"s/'//" \
	             | awk '{print $$3}')
	echo "\newcommand{\maDataUnit}{$$v}" >> $@.tmp

        # In the end, set the final name of this file.
	mv $@.tmp $@





# Ultimate target (prerequisite of 'all').
report.pdf: report.tex tex/*.tex \
            $(texmacros) \
            $(vis-full) \
            $(vis-diffuse) | $(tikzdir)
	curdir=$$(pwd)
	cp -r report.tex tex/ $(texdir)/
	cd $(texdir)
	latex -shell-escape -halt-on-error report.tex
	biber report
	latex -shell-escape -halt-on-error report.tex
	dvipdf report.dvi
	cp report.pdf $$curdir/
